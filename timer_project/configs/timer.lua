-- в Clock хранится текущее время в виде секунд - seconds и минут - minutes, а также интервал таймера в миллисекундах - interval и статус таймера - on = false|true
Clock = {seconds = 0; -- секунды
         minutes = 0; -- минуты
         interval = 1000; -- интервал обновления
         on = false; -- статус таймера
         }

-- При нажатии на кнопку Старт - включается таймеры tick и lamp и текст кнопки становится Стоп
-- При нажатии на кнопку Стоп - таймер tick выключается и текст кнопки становится Старт
function H_startstoptimer(ev)
  if Clock.on == false then
    Clock.on = true
    SetText('main.btStartStop.text.Text', 'txtStop')
	-- Включаем таймер на Clock.interval с цикличным срабатыванием (once = false)
    sys.StartTimer("tick", Clock.interval, false)
	-- Включаем лампу и одноразовый таймер на 5с
	Set('main.Lamp.state.', 'on')
    sys.StartTimer("lamp", 5000)
  else
    Clock.on = false
    SetText('main.btStartStop.text.Text', 'txtStart')
	-- Останавливаем цикличный таймер
    sys.StopTimer("tick")
  end
end
AddEventHandler('main.btStartStop.Mouse.LeftPress', 'H_startstoptimer')

-- По таймеру tick обновляются часы
function H_refresh(ev)
  Clock.seconds = Clock.seconds + Clock.interval/1000
  if Clock.seconds > 60 then 
    Clock.minutes = Clock.minutes + 1
    Clock.seconds = Clock.seconds - 60
  end
  if Clock.minutes > 60 then 
    Clock.minutes = Clock.minutes - 60
  end
  -- Устанавливаем значение цифровых часов
  Set("main.Clock.value.", string.format("%02d%02d", Clock.minutes, math.floor(Clock.seconds)))
end
AddEventHandler('timer.tick.finished.', 'H_refresh')

-- По таймеру lamp выключается лампа
function H_lamp(ev)
	Set('main.Lamp.state.', 'off')
end
AddEventHandler('timer.lamp.finished.', 'H_lamp')

